//
//  DetailsViewController.swift
//  IntroductionToCoreData
//
//  Created by Mohammad Arif Hossain on 11/10/18.
//  Copyright © 2018 Recursion Technologies. All rights reserved.
//

import UIKit
import CoreData

class DetailsViewController: UIViewController {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var nameData: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var numberData: UILabel!
    
    @IBOutlet weak var backButton: UIButton!

    var people : NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Details"
        nameData.text = people?.value(forKey: "name") as! String
        
        let number = people?.value(forKey: "number")
        numberData.text = String(describing: number!)
    }


}
