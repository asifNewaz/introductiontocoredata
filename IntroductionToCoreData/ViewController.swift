//
//  ViewController.swift
//  IntroductionToCoreData
//
//  Created by Mohammad Arif Hossain on 11/8/18.
//  Copyright © 2018 Recursion Technologies. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var names: [String] = []
    var peoples: [NSManagedObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "The List"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //1
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
        
        //3
        do {
            peoples = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var txtFieldName: UITextField!
    var txtFieldNumber: UITextField!
    
    func txtFieldName(textField: UITextField!)
    {
        textField.placeholder = "Enter name"
        textField.keyboardType = .asciiCapable
        txtFieldName = textField
    }
    
    func txtFieldNumber(textField: UITextField!)
    {
        textField.placeholder = "Enter number"
        textField.keyboardType = .asciiCapableNumberPad
        txtFieldNumber = textField
    }
    
    // Implement the addName IBAction
    @IBAction func addName(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "New contact",message: "", preferredStyle: .alert)
       
        alert.addTextField(configurationHandler: txtFieldName)
        alert.addTextField(configurationHandler: txtFieldNumber)
        
    
        let saveAction = UIAlertAction(title: "Save", style: .default) {
            [unowned self] action in
            
            print("Name : \(self.txtFieldName.text!)")
            print("Number : \(self.txtFieldNumber.text!)")
            
            guard let nameToSave = self.txtFieldName.text,  !nameToSave.isEmpty else {
                    return
            }
            
            guard let numberToSave = self.txtFieldNumber.text,  !numberToSave.isEmpty else {
                return
            }
            
            self.save(name: nameToSave, number: numberToSave)
            self.tableView.reloadData()
        }

        
        let cancelAction = UIAlertAction(title: "Cancel",style: .cancel)
        
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }
    
    func save(name: String, number: String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Person", in: managedContext)!
        
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        
        // 3
        person.setValue(name, forKeyPath: "name")
        
        let numberInt: Int = Int(number)!
        person.setValue(numberInt, forKeyPath: "number")
        
        // 4
        do {
            try managedContext.save()
            peoples.append(person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }


}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,numberOfRowsInSection section: Int) -> Int {
        return peoples.count
    }
    
    func tableView(_ tableView: UITableView,cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let person = peoples[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell",for: indexPath)
        cell.textLabel?.text = person.value(forKeyPath: "name") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
        nextVC?.people = peoples[indexPath.row]
        self.navigationController?.show(nextVC!, sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Delete"
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let people = self.peoples[indexPath.row]
        var actionString  = "Delete"
    
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            let refreshAlert = UIAlertController(title: "Delete", message: "Are you want to delete contact?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                
                self.deleteContact(people: people, index:  indexPath)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func deleteContact(people:NSManagedObject, index: IndexPath) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext = appDelegate.persistentContainer.viewContext

        managedContext.delete(people as NSManagedObject)
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error While Deleting Note: \(error.userInfo)")
        }
        
        self.peoples.remove(at: index.row)
        
        self.tableView.deleteRows(at: [index], with: .fade)
    }
}
