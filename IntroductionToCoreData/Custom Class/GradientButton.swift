//
//  GradientButton.swift
//  IntroductionToCoreData
//
//  Created by Mohammad Arif Hossain on 11/10/18.
//  Copyright © 2018 Recursion Technologies. All rights reserved.
//

import UIKit

@IBDesignable
class GradientButton: UIButton {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    private func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }
}

/*Gradient with rounded border */

@IBDesignable
class GradientWithRoundedButton: UIButton {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    private func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }
    
    
    // MARK:- Properties
    @IBInspectable var cornerRadious: CGFloat = 5 {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var textColor: UIColor = UIColor.white {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var customBackgroundColor: UIColor = UIColor.colorFrom(hexString: "2E7D32")! {
        didSet{
            updateUI()
        }
    }
    
    @IBInspectable var fontSize: CGFloat = 20 {
        didSet{
            updateUI()
        }
    }
    
    // MARK:- Methods
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 15)
        self.layer.cornerRadius = cornerRadious
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        
        self.layer.shadowRadius = 4
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 6)
        self.layer.shadowOpacity =  0.14
        self.layer.shadowPath = shadowPath.cgPath
        
        self.clipsToBounds = true
        self.tintColor = textColor
        self.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = customBackgroundColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = customBackgroundColor
    }
    
    func updateUI() {
        self.setNeedsDisplay()
    }

}
